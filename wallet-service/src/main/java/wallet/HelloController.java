package wallet;

import static io.micronaut.core.util.CollectionUtils.mapOf;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.views.View;

@Controller()
public class HelloController {
  @Get(uri = "hello", produces = MediaType.TEXT_PLAIN)
  public String index() {
    return "Hello World";
  }

  @View("home")
  @Get("/")
  public HttpResponse home() {
    return HttpResponse.ok(mapOf(
        "loggedIn", true,
        "username", "sdelamo"));
  }

}
